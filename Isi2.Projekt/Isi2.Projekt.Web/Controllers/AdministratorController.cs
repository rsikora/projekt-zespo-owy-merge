﻿using Isi2.Projekt.Contracts;
using Isi2.Projekt.Web.Code;
using Isi2.Projekt.Web.Helpers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Isi2.Projekt.Web.Controllers
{
    [Authorize]
    [AccessGroup]
    public class AdministratorController : Controller
    {
        //
        // GET: /Admnistrator/
        private readonly IService _service = WindsorContainerAccess.GetContainer().Resolve<IService>();


        public ActionResult Index()
        {
            UserContract model = default(UserContract);
            var users = _service.GetUsers(new Contracts.User.UserFilterContract() {Login = User.Identity.Name},
                                          new Contracts.Common.PagedListRequestContract() {Take = 1});
            if (users != null)
            {
                model = users.Items.First();
            }
            return View(model);
        }

        [Authorize]
        public ActionResult BanUser(int userId)
        {
            var model =
                _service.GetUsers(new Contracts.User.UserFilterContract() {Id = userId}, null).Items.FirstOrDefault();
            return PartialView("_banUser", model);
        }

        [Authorize]
        public ActionResult UnBanUser(int userId)
        {
            var model =
                _service.GetUsers(new Contracts.User.UserFilterContract() {Id = userId}, null).Items.FirstOrDefault();
            return PartialView("_UnbanUser", model);
        }

        [Authorize]
        public ActionResult UnBan(int userId)
        {
            string message;
            string status;
            if (_service.UnBanUser(userId))
            {
                status = "OK";
                message = "Pomyślnie odbanowano użytkownika";
            }
            else
            {
                status = "ERROR";
                message = "Wystąpił błąd. Spróbuj później ;)";
            }
            return Json(new {status = status, message = message}, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public ActionResult Ban(int userId, string banReason)
        {
            if (string.IsNullOrWhiteSpace(banReason))
            {
                return Json(new {status = "ERROR", message = "Musisz podać powód zbanowania."},
                            JsonRequestBehavior.AllowGet);
            }
            string message;

            string status;
            if (_service.BanUser(userId, banReason))
            {
                status = "OK";
                message = "Banowanie użytkownika zakończone sukcesem.";
            }
            else
            {
                status = "ERROR";
                message = "Banowanie użytkownika zakończone błędem. ";
            }
            return Json(new {status = status, message = message}, JsonRequestBehavior.AllowGet);
        }

        [Authorize]
        public JsonResult Users(string term)
        {
            var userDict = _service.Users(term);
            var jsonList = new List<object>();
            if (userDict.Count > 0)
            {
                foreach (var item in userDict)
                {
                    jsonList.Add(new
                        {
                            id = item.Key,
                            label = item.Value,
                            value = item.Value
                        });
                }
            }
            return Json(jsonList.ToArray(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult UserCard(int id)
        {
            UserContract model = default(UserContract);
            var users = _service.GetUsers(new Contracts.User.UserFilterContract() {Id = id},
                                          new Contracts.Common.PagedListRequestContract() {Take = 1});
            if (users != null)
            {
                model = users.Items.First();
            }
            return PartialView("_UserCard", model);
        }

        public ActionResult SetPermissions(int userId)
        {
            var model =
                _service.GetUsers(new Contracts.User.UserFilterContract() {Id = userId}, null).Items.FirstOrDefault();
            return PartialView("_setPermissions", model);
        }

        [HttpPost]
        public ActionResult SetPermissions(int userId, bool isMod, bool isAdmin)
        {
            string message = string.Empty;
            string status = string.Empty;
            if (_service.SetPermissions(userId, isMod, isAdmin))
            {
                status = "OK";
                message = "Nadawanie praw użytkownika zakończone sukcesem. ";
            }
            else
            {
                status = "ERROR";
                message = "Nadawanie praw użytkownika zakończone błędem. ";
            }
            return Json(new {message = message, status = status}, JsonRequestBehavior.AllowGet);
        }
    }
}