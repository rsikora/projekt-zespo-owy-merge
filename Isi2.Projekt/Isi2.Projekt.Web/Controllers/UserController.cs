﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Isi2.Projekt.Contracts;
using Isi2.Projekt.Contracts.Common;
using Isi2.Projekt.Contracts.Entry;
using Isi2.Projekt.Web.Helpers;
using Isi2.Projekt.Web.Models.ViewModels;

namespace Isi2.Projekt.Web.Controllers
{
    public class UserController : Controller
    {
        //
        // GET: /User/
        private IService _service = WindsorContainerAccess.GetContainer().Resolve<IService>();
        public ActionResult Index(string userName ="", int page = 1)
        {

            if (string.IsNullOrWhiteSpace(userName))
            {
                userName = User.Identity.Name;
            }
            UserViewModel model = new UserViewModel();
            UserContract user = default(UserContract);
            var users = _service.GetUsers(new Contracts.User.UserFilterContract() { Login = userName },
                                          new Contracts.Common.PagedListRequestContract() { Take = 1 });
            if (users != null)
            {
                user = users.Items.First();
                model.User = user;
            }
            else
            {
                return View("UserNotFound");
            }
            var userEntries = _service.GetEntries(new EntryFilterContract() {UserId = model.User.Id},
                                                 new PagedListRequestContract {Page = page - 1, PageSize = 5, Take = 5});
            
            model.Entries = userEntries;
            return View("Profile", model);
        }

    }
}
